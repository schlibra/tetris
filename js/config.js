var strict = false;/*严格游戏模式，开启时在暂停游戏后无法移动方块*/
var runTimeout=200;/*运动一次的时间（ms），数值越小速度越快*/
var oneBlockScore=1;/*每个方块消除后的得分*/
var mode="game";/*运行模式： dev=开发模式，game=游玩模式*/